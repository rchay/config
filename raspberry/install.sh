#!/bin/bash

sudo apt update
sudo apt -y upgrade

echo 'export PATH=/home/pi/.local/bin/:$PATH' >> ~/.bashrc
source ~/.bashrc

sudo bash -c 'echo LC_CTYPE=en_US.UTF-8 >> /etc/default/local'
sudo bash -c 'echo LC_MESSGES=en_US.UTF-8 >> /etc/default/local'
sudo bash -c 'echo LC_ALL=en_US.UTF-8 >> /etc/default/local'

python3 -m pip install --upgrade pip
python3 -m pip install --upgrade setuptools

curl https://gitlab.com/rchay/config/-/raw/master/python/requirements.txt > requirements.txt
python3 -m pip install -r requirements.txt
rm requirements.txt

sudo curl https://gitlab.com/rchay/config/-/raw/master/raspberry/keyboard > /etc/default/keyboard

sudo apt install -y idle
sudo apt install -y cmake
sudo apt install -y mypaint
sudo apt install -y sqlitebrowser
sudo apt install -y imagemagick

sudo apt remove -y thonny

pip install mkdocs mkdocs-material mkdocs-macros-plugin
source ~/.bashrc
git clone https://gitlab.com/lfbogota/fablab.git /home/pi/fablab/
cd /home/pi/fablab/
mkdocs build

curl https://gitlab.com/rchay/config/-/raw/master/raspberry/Fablab.desktop > /home/pi/Desktop/Fablab.desktop

curl https://sonic-pi.net/files/releases/v3.3.1/sonic-pi_3.3.1_1_armhf.deb > sonicpi.deb
sudo apt install -y ~/sonicpi.deb
rm sonicpi.deb


sudo apt autoremove -y
sudo apt clean -y



cd ~
sudo pip3 install --upgrade adafruit-python-shell
wget https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/raspi-blinka.py
sudo python3 raspi-blinka.py

pip3 install adafruit-circuitpython-dht
sudo apt-get install libgpiod2

