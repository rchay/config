Configuration des postes info pour SNT / NSI
- python : librairies à installer (`pip install -r .\requirements.txt`)
- software : logiciels à installer



Environnements virtuels :
- installer virtualenv : `pip install virtualenv`
- créer un nouvel environnement virtuel appelé `venv` : `virtualenv venv`
- activer l'environnement virtuel : `.\venv\Scripts\activate`
- désactiver l'environnement virtuel : `deactivate`


Télécharment :
- Powershell : `Invoke-WebRequest -Uri https://gitlab.com/rchay/config/-/blob/master/python/requirements.txt -OutFile requirements.txt`
- Linux : `curl -O https://gitlab.com/rchay/config/-/blob/master/python/requirements.txt -OutFile requirements.txt`