# Python

- Python + IDLE (https://www.python.org/downloads/)
- Thonny (https://thonny.org/)
- Graphviz (https://www.graphviz.org/download/)
- VSCodium (https://vscodium.com/)

# Web

- Notepad ++ (https://notepad-plus-plus.org/downloads/)

# Arduino

- IDE Arduino (https://www.arduino.cc/en/Main/Software)

# Réseaux

- Putty (https://putty.org/)
- Wireshark (https://www.wireshark.org/download.html)
- Filius (https://lernsoftware-filius.de/Herunterladen)
- Advanced IP Scanner (https://www.advanced-ip-scanner.com/fr/)
- Filezilla (https://filezilla-project.org/download.php?type=client)

# Circuits

- Logisim (https://sourceforge.net/projects/circuit/)

# BDD

- SQL Lite Browser (https://sqlitebrowser.org/)
- Heidi SQL (https://www.heidisql.com/download.php)

# Bureautique

- Mozilla Firefox
- LibreOffice (https://www.libreoffice.org/download/download/)
- GIMP (https://www.gimp.org/downloads/)
