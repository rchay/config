# environnment
pytest
jupyterlab
nbgrader
jupyter_contrib_nbextensions
RISE
pandoc
black
metakernel


# calcul scientifique
numpy
pandas
seaborn
scikit-learn
matplotlib
graphviz


# developement de jeux et interfaces graphiques
pygame
pyxel


# web
flask
django
bottle
requests
beautifulsoup4


# objet connecté
serial
gpiozero
Mastodon.py

# openstreetmap
folium
pyroutelib3


# graphique
pillow
imageio
graphviz

